//
//  LoginViewController.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import UIKit
import SVProgressHUD
import SwiftMessages

class LoginViewController: UIViewController, ServiceUsuarioDelegate  {

    var service : ServiceUsuario!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var senhaTextField: UITextField!
    
    @IBOutlet weak var botaoEntrar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.botaoEntrar.layer.cornerRadius = 5
        
        self.service = ServiceUsuario(delegate: self)
    }
    
    @IBAction func login(_ sender: Any) {
        
        if let email = emailTextField.text, !email.isEmpty, let senha = senhaTextField.text, !senha.isEmpty {
            
            SVProgressHUD.show(withStatus: "Carregando")
            
            SVProgressHUD.setDefaultMaskType(.black)
            
            self.service.postLoginUsuario(email: email, senha: senha)
            
        } else {
            
           Alerta.show(message: "Campos ausentes",target: self)
        }
    }

    
    func postLoginSuccess() {
        
        SVProgressHUD.dismiss()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func postLoginFailure(error: String?) {
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            
            Alerta.show(message: error, target: self)
        }
    }
}
