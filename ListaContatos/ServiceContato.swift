//
//  ServiceContato.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire

protocol ServiceContatoDelegate {
    
    func postCriarContatoSuccess()
    func postCriarContatoFailure(error: String?)
    
    func putEditarContatoSuccess()
    func putEditarContatoFailure(error: String?)
    
    func deleteContatoSuccess(indexPath: IndexPath)
    func deleteContatoFailure(error: String?)
    
    func getContatoSuccess()
    func getContatoFailure(error: String?)
}

extension ServiceContatoDelegate {
    
    func postCriarContatoSuccess() {
        fatalError(#function)
    }
    
    func postCriarContatoFailure(error: String?) {
        fatalError(#function)
    }
    
    func putEditarContatoSuccess() {
        fatalError(#function)
    }
    
    func putEditarContatoFailure(error: String?) {
        fatalError(#function)
    }
    
    func deleteContatoSuccess(indexPath: IndexPath) {
        fatalError(#function)
    }
    
    func deleteContatoFailure(error: String?) {
        fatalError(#function)
    }
    
    func getContatoSuccess() {
        fatalError(#function)
    }
    
    func getContatoFailure(error: String?) {
        fatalError(#function)
    }
}

class ServiceContato {
    
    var delegate : ServiceContatoDelegate!
    
    var request: Request?
    
    init(delegate : ServiceContatoDelegate) {
        
        self.delegate = delegate
    }
    
    func postCriarContato(contato: Contato) {
        
        self.request?.cancel()
        
        self.request = RequestFactory.postCriarContato(contato: contato).responseObject(keyPath: "data",  completionHandler: { (response: DataResponse<Contato>) in
            
            switch response.result {
                
            case .success:
                
                if let contato = response.result.value {
                    
                    Contato.save(contato: contato)
                    
                    self.delegate.postCriarContatoSuccess()
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.postCriarContatoFailure(error: errorMessage)
            }
        })
    }
    
    func putEditarContato(contato: Contato) {
        
        self.request?.cancel()
        
        self.request = RequestFactory.putEditarContato(contato: contato).responseObject(keyPath: "data", completionHandler: { (response: DataResponse<Contato>) in
            
            switch response.result {
                
            case .success:
                
                if let contato = response.result.value {
                    
                    Contato.save(contato: contato)
                    
                    self.delegate.putEditarContatoSuccess()
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.putEditarContatoFailure(error: errorMessage)
            }
        })
    }
    
    func deleteContato(id: Int, indexPath: IndexPath) {
        
        self.request?.cancel()
        
        self.request = RequestFactory.deleteContato(id: id).responseObject(completionHandler: { (response: DataResponse<Contato>) in
            
            switch response.result {
                
            case .success:
                
                if let _ = response.result.value {
                    
                    self.delegate.deleteContatoSuccess(indexPath: indexPath)
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.deleteContatoFailure(error: errorMessage)
            }
        })
    }
    
    func getContatos() {
        
        self.request?.cancel()
        
        self.request = RequestFactory.getContatos().responseArray(keyPath: "data", completionHandler: { (response: DataResponse<[Contato]>) in
            
            switch response.result {
                
            case .success:
                
                if let contatos = response.result.value {
                    
                    Contato.deleteAll()
                    
                    Contato.saveAll(contatos: contatos)
                    
                    self.delegate.getContatoSuccess()
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.getContatoFailure(error: errorMessage)
            }
        })
    }
}

