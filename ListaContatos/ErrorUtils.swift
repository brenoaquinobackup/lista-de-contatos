//
//  ErrorHandler.swift
//  ListaContatos
//
//  Created by Thiago Diniz on 25/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire

class ErrorUtils {
    
    static func errorHandler<T: Any>(response: DataResponse<T>) -> String? {
        
        var errorMessage: String?
        
        if let httpResponse = response.response {
            
            switch httpResponse.statusCode {
                
            case 500...599 :
                
                errorMessage = "Erro Interno, tente novamente mais tarde."
                
                print("Erro de servidor : \(response.result.error.debugDescription)")
                
            case 422:
                
                if let data = response.data {
                    
                    let errorJson = JSON(data: data)
                    
                    errorMessage = self.getMessages(messages: errorJson["errors"]["full_messages"].arrayObject)
                }
                
            case 401:
                
                if let data = response.data {
                    
                    let errorJson = JSON(data: data)
                    
                    errorMessage = self.getMessages(messages: errorJson["errors"].arrayObject)
                }
            case 404:
                
                errorMessage = "Não encontrado."
                
            default:
                
                errorMessage = "Algo de errado aconteceu!"
            }
            
        } else {
            
            if let error = response.result.error as? URLError {
                
                if error.errorCode == -1009 {
                    
                    errorMessage = "Verifique sua conexão e tente novamente!"
                    
                } else if error.errorCode == -999 {
                    
                    print("Request Cancelado : \(error)")
                    
                } else {
                    
                    errorMessage = "Algo de errado aconteceu!"
                }
            }
        }
        
        return errorMessage
    }
    
    fileprivate static func getMessages(messages: [Any]?) -> String? {
        
        var result = ""
        
        if let messages = messages {
            
            for (index,message) in messages.enumerated() {
                
                if let value = message as? String, !value.isEmpty {
                    
                    result = result + "∙ \(message)"
                    
                    if index != messages.count-1 {
                        
                       result = result + "\n"
                    }
                }
            }
            
            return result
        }
        
        return nil
    }
}
