//
//  EditarContatoViewController.swift
//  ListaContatos
//
//  Created by administrador on 26/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditarContatoViewController: UIViewController, ServiceContatoDelegate {

    @IBOutlet weak var nomeContato: UITextField!
    @IBOutlet weak var numeroContato: UITextField!
    @IBOutlet weak var emailContato: UITextField!
    @IBOutlet weak var apelidoContato: UITextField!
    @IBOutlet weak var dataNascimentoContato: UITextField!
    @IBOutlet weak var imagemContato: UITextField!
   
    @IBOutlet weak var botao: UIButton!
    
    var contato: Contato?
    
    var service: ServiceContato!
    
    var idUsuarioLogado: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.idUsuarioLogado = Usuario.get()!.id
        
        self.service = ServiceContato(delegate: self)
        
        if let contato = self.contato {
            
            self.preencherCampos(contato)
            
            self.title = contato.nome
            
            self.botao.setTitle("Salvar", for: .normal)
            
        } else {
            
            self.title = "Novo Contato"
            
            self.botao.setTitle("Criar", for: .normal)
        }
    }
    
    func dateToString(date: Date?) -> String? {
     
        if let date = date {
        
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "dd/MM/yyyy"
            
            return dateFormatter.string(from: date)
        }
        
        return nil
    }
    
    func preencherCampos(_ contato: Contato) {
        
        self.nomeContato.text = contato.nome
        self.numeroContato.text = contato.numero
        self.emailContato.text = contato.email
        self.apelidoContato.text = contato.apelido
        self.dataNascimentoContato.text = self.dateToString(date: contato.nascimento)
        self.imagemContato.text = contato.imagem
    }
    
    //MARK: Actions
    
    @IBAction func salvar(_ sender: Any) {
        
        let contatoEditado = Contato()
        
        if let nome = self.nomeContato.text, !nome.isEmpty {
            
            contatoEditado.nome = nome
        }
        
        if let numero = self.numeroContato.text, !numero.isEmpty {
            
            contatoEditado.numero = numero
        }
        
        if let email = self.emailContato.text, !email.isEmpty {
            
            contatoEditado.email = email
        }

        if let apelido = self.apelidoContato.text, !apelido.isEmpty {
            
            contatoEditado.apelido = apelido
        }
        
        if let dataNascimento = self.dataNascimentoContato.text {
            
            let formatter = DateFormatter()
            
            formatter.dateFormat = "dd/MM/yyyy"
            
            contatoEditado.nascimento = formatter.date(from: dataNascimento)
        }
        
        if let imagem = self.imagemContato.text, !imagem.isEmpty {
            
            contatoEditado.imagem = imagem
        }

        contatoEditado.usuario = self.idUsuarioLogado
        
        SVProgressHUD.show()
        
        SVProgressHUD.setDefaultMaskType(.black)
        
        if let contato = self.contato {
            
            contatoEditado.id.value = contato.id.value
            
            self.service.putEditarContato(contato: contatoEditado)
            
        } else {
            
            self.service.postCriarContato(contato: contatoEditado)
        }
    }
    
    @IBAction func editDataNascimento(_ sender: UITextField) {
        
        let datePickerView: UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(EditarContatoViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        
        self.dataNascimentoContato.text = self.dateToString(date: sender.date)
    }
    
    //MARK: Requests
    
    func postCriarContatoSuccess() {
        
        SVProgressHUD.dismiss()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func postCriarContatoFailure(error: String?) {
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            
            self.showMessage(message: error)
        }
    }
    
    func putEditarContatoSuccess() {
        
        SVProgressHUD.dismiss()
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func putEditarContatoFailure(error: String?) {
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            
            self.showMessage(message: error)
        }
    }
    
    func showMessage(message: String) {
        
        let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .alert)
        
        let closeAction = UIAlertAction(title: "Fechar", style: .default, handler: nil)
        
        alert.addAction(closeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}
