//
//  Usuario.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Usuario: Object, Mappable {
    
    dynamic var id = 0
    dynamic var email = ""
    dynamic var token = ""
    dynamic var client = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        email <- map["email"]
    }
}

extension Usuario {

    static func get() -> Usuario? {
    
        return realmInstance.objects(Usuario.self).first
    }
    
    static func save(usuario: Usuario) {
        
        try! realmInstance.write {
            
            realmInstance.add(usuario, update: true)
        }
    }
}
