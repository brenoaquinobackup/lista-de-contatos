//
//  DateTransform.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import ObjectMapper

class DateTransform: TransformType {
    
    typealias Object = Date
    typealias JSON = String
    var formatter: DateFormatter
    
    init() {
        
        self.formatter = DateFormatter()
        self.formatter.dateFormat = "yyyy-MM-dd"
    }

    func transformFromJSON(_ value: Any?) -> Date? {
        
        if let dateString = value as? String {
            
            return self.formatter.date(from: dateString)
            
        }
        
        return nil
    }
    
    func transformToJSON(_ value: Date?) -> String? {
        
        if let value = value {
            
            return self.formatter.string(from: value)
        }
        
        return nil
    }
}
