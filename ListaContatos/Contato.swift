//
//  Contato.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Contato : Object, Mappable {
    
    let id = RealmOptional<Int>()
    dynamic var nome: String?
    dynamic var nascimento: Date?
    dynamic var email: String?
    dynamic var numero: String?
    dynamic var apelido: String?
    dynamic var imagem: String?
    dynamic var usuario = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        
        id.value <- map["id"]
        nome <- map["name"]
        nascimento <- (map["birth_date"], DateTransform())
        email <- map["email"]
        numero <- map["phone_number"]
        apelido <- map["nickname"]
        imagem <- map["image_url"]
        usuario <- map["user_id"]
    }
}

extension Contato {
    
    static func all() -> [Contato] {
    
        var contatos: [Contato] = []
        
        let results = realmInstance.objects(Contato.self).sorted(byKeyPath: "nome", ascending: true)
        
        contatos.append(contentsOf: results)
        
        return contatos
    }
    
    static func allGrouped() -> [[Contato]] {
        
        let todosContatos = self.all()
        
        var contatosOrdenados: [[Contato]] = []
        
//        var todasIniciais = todosContatos.map { (contato: Contato) -> String in
//            
//            return String(describing: contato.nome.characters.first)
//        }
        
        let todasIniciais = todosContatos.map({String(describing: $0.nome?.characters.first)})
        
        var iniciais: [String] = []
        
        for inicial in todasIniciais {
            
            if !iniciais.contains(inicial) {
                
                iniciais.append(inicial)
            }
        }
        
        for inicial in iniciais {
            
            let resultado = todosContatos.filter({String(describing: $0.nome?.characters.first) == inicial})
            
            if !resultado.isEmpty {
                
                contatosOrdenados.append(resultado)
            }
        }
        
        return contatosOrdenados
    }
    
    static func save(contato: Contato) {
        
        try! realmInstance.write {
            
            realmInstance.add(contato, update: true)
        }
    }
    
    static func saveAll(contatos: [Contato]) {
        
        try! realmInstance.write {
            
            realmInstance.add(contatos, update: true)
        }
    }
    
    static func deleteAll() {
        
        try! realmInstance.write {
            
            let todosContatos = realmInstance.objects(Contato.self)
            
            realmInstance.delete(todosContatos)
        }
    }
    
    static func delete(id: Int) {
        
        if let contato = realmInstance.object(ofType: Contato.self, forPrimaryKey: id) {
            
            try! realmInstance.write {
                
                realmInstance.delete(contato)
            }
        }
    }
}
