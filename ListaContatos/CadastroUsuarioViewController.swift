//
//  CadastroUsuarioViewController.swift
//  ListaContatos
//
//  Created by administrador on 25/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import UIKit
import SVProgressHUD

class CadastroUsuarioViewController: UIViewController, ServiceUsuarioDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var senhaTextField: UITextField!
    
    @IBOutlet weak var senhaConfirmaTextField: UITextField!
   
    var service: ServiceUsuario!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.service = ServiceUsuario(delegate: self)
    }
    
    @IBAction func cadastrar(_ sender: Any) {
        
        if let email = emailTextField.text, !email.isEmpty, let senha = senhaTextField.text, !senha.isEmpty, let senhaConfirma = senhaConfirmaTextField.text, !senhaConfirma.isEmpty {
            
            if senha != senhaConfirma {
                
                self.showMessage(message: "Senhas não conferem")
                
            } else {
                
                SVProgressHUD.show()
                
                SVProgressHUD.setDefaultMaskType(.black)
                
                self.service.postCriarUsuario(email: email, senha: senha)
            }
            
        } else {
            
            self.showMessage(message: "Campos ausentes")
        }
    }
    
    
    func showMessage(message: String) {
        
        let alert = UIAlertController(title: "Atenção", message: message, preferredStyle: .alert)
        
        let closeAction = UIAlertAction(title: "Fechar", style: .default, handler: nil)
        
        alert.addAction(closeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Requests
    
    func postCriarUsuarioSuccess() {
        
        SVProgressHUD.dismiss()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func postCriarUsuarioFailure(error: String?) {
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            
            self.showMessage(message: error)
        }
    }
}
