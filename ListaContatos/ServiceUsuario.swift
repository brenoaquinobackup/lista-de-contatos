//
//  ServiceUsuario.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire

protocol ServiceUsuarioDelegate {
    
    func postCriarUsuarioSuccess()
    func postCriarUsuarioFailure(error: String?)
    
    func postLoginSuccess()
    func postLoginFailure(error: String?)
    
    func deleteLogoutSuccess()
    func deleteLogoutFailure(error: String?)
}

extension ServiceUsuarioDelegate {
    
    func postCriarUsuarioSuccess() {
        fatalError(#function)
    }
    
    func postCriarUsuarioFailure(error: String?) {
        fatalError(#function)
    }
    
    func postLoginSuccess() {
        fatalError(#function)
    }
    
    func postLoginFailure(error: String?) {
        fatalError(#function)
    }
    
    func deleteLogoutSuccess() {
        fatalError(#function)
    }
    
    func deleteLogoutFailure(error: String?) {
        fatalError(#function)
    }
}

class ServiceUsuario {
    
    var delegate : ServiceUsuarioDelegate!
    
    var request: Request?
    
    init(delegate : ServiceUsuarioDelegate) {
    
        self.delegate = delegate
    }
    
    func postLoginUsuario(email: String, senha: String) {
        
        self.request?.cancel()
        
        self.request = RequestFactory.postLogin(email: email, senha: senha).responseObject(keyPath: "data") { (response: DataResponse<Usuario>) in
            
            switch response.result {
                
            case .success:
                
                if let usuario = response.result.value {
                    
                    let credentials = self.getCredentials(response.response)
                    
                    usuario.token = credentials.token
                    
                    usuario.client = credentials.client
                    
                    Usuario.save(usuario: usuario)
                    
                    self.delegate.postLoginSuccess()
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.postLoginFailure(error: errorMessage)
            }
        }
    }
    
    func deleteLogout(email: String) {
        
        self.request?.cancel()
        
        self.request = RequestFactory.deleteLogout(email: email).responseJSON { (response: DataResponse<Any>) in
            
            switch response.result {
                
            case .success:
                
                if let _ = response.result.value {
                    
                    try! realmInstance.write {
                        
                        realmInstance.deleteAll()
                    }
                    
                    self.delegate.deleteLogoutSuccess()
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.deleteLogoutFailure(error: errorMessage)
            }
        }
    }
    
    func postCriarUsuario(email: String, senha: String) {
        
        self.request?.cancel()
        
        self.request = RequestFactory.postCriarUsuario(email: email, senha: senha).responseObject(keyPath: "data") { (response: DataResponse<Usuario>) in
            
            switch response.result {
                
            case .success:
                
                if let usuario = response.result.value {
                    
                    let credentials = self.getCredentials(response.response)
                    
                    usuario.token = credentials.token
                    
                    usuario.client = credentials.client
                    
                    Usuario.save(usuario: usuario)
                    
                    self.delegate.postCriarUsuarioSuccess()
                }
                
            case .failure:
                
                let errorMessage = ErrorUtils.errorHandler(response: response)
                
                self.delegate.postCriarUsuarioFailure(error: errorMessage)
            }
        }
    }
    
    fileprivate func getCredentials(_ response: HTTPURLResponse?) -> (token: String, client: String) {
        
        var credentials = (token: "", client: "")
        
        if let response = response {
            
            if let token = response.allHeaderFields["Access-Token"] as? String {
                
                credentials.token = token
            }
            
            if let client = response.allHeaderFields["Client"] as? String {
                
                credentials.client = client
            }
        }
        
        return credentials
    }
}
