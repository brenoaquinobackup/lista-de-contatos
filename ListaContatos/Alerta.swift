//
//  Alerta.swift
//  ListaContatos
//
//  Created by administrador on 28/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import SwiftMessages

class Alerta {
    
    static func show(message: String, target: UIViewController) {
        
        let view = MessageView.viewFromNib(layout: .MessageView)
        
        view.configureTheme(.error)
        
        view.bodyLabel?.text = message
        view.titleLabel?.isHidden =  true
        view.button?.isHidden = true
        view.iconImageView?.isHidden = true
        view.iconLabel?.isHidden = true
        
        view.backgroundColor = UIColor(red: 249.0/255.0, green: 66.0/255.0, blue: 47.0/255.0, alpha: 1.0)
        
        var config = SwiftMessages.Config()
        
        config.presentationContext = .viewController(target)
        
        SwiftMessages.show(config: config, view: view)
    }
}
