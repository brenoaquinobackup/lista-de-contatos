//
//  ViewController.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ServiceContatoDelegate, ServiceUsuarioDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var contatos: [[Contato]] = []
    
    var service: ServiceContato!
    
    var serviceUsuario: ServiceUsuario!
    
    //MARK: Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Contatos"
        
        self.service = ServiceContato(delegate: self)
        
        self.serviceUsuario = ServiceUsuario(delegate: self)
        
        self.contatos = Contato.allGrouped()
        
        if let _ = Usuario.get() {

            self.loadData()
            
        } else {

            self.performSegue(withIdentifier: "login", sender: self)
        }
 
        self.tableView.register(UINib(nibName: "ContatoTableViewCell", bundle: nil), forCellReuseIdentifier: "contatoCell")
        
        self.tableView.tableFooterView = UIView()
    }
    
    func loadData() {
        
        self.service.getContatos()
        
        self.contatos = Contato.allGrouped()
        
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       self.loadData()
    }
    
    //MARK: Tableview Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.contatos.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.contatos[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let contato = self.contatos[indexPath.section][indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "contatoCell", for: indexPath) as! ContatoTableViewCell

        cell.nomeContato.text = contato.nome
        
        cell.numeroContato.text = contato.numero
        
        if let image = contato.imagem {
            
            cell.imageContato.kf.setImage(with: URL(string: image))
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 65
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let contato = self.contatos[indexPath.section][indexPath.row]
        
        self.performSegue(withIdentifier: "editarContato", sender: contato)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let contatos = self.contatos[section]
        
        let primeiroContato = contatos.first
        
        if let primeiroContato = primeiroContato {
            
            if let letra = primeiroContato.nome?.characters.first {
                
                return String(letra).uppercased()
            }
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            
            let contato = self.contatos[indexPath.section][indexPath.row]
            
            self.service.deleteContato(id: contato.id.value!, indexPath: indexPath)
        }
    }
    
    //MARK: Actions
    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController(title: "Atenção", message: "Deseja fazer o logout?", preferredStyle: .alert)
        
        let closeAction = UIAlertAction(title: "Fechar", style: .cancel, handler: nil)
        
        let okAction = UIAlertAction(title: "Sim", style: .default) { (alert: UIAlertAction) in
            
            SVProgressHUD.show()
            
            SVProgressHUD.setDefaultMaskType(.black)
            
            let usuarioLogado = Usuario.get()!
            
            self.serviceUsuario.deleteLogout(email: usuarioLogado.email)
        }
        
        alert.addAction(closeAction)
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Requests
    
    func getContatoSuccess() {
        
        self.contatos = Contato.allGrouped()
        
        self.tableView.reloadData()
    }
    
    func getContatoFailure(error: String?) {
        
        if let error = error {
            
            let alert = UIAlertController(title: "Atenção", message: error, preferredStyle: .alert)
            
            let closeAction = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            
            alert.addAction(closeAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func deleteLogoutSuccess() {
        
        SVProgressHUD.dismiss()
        
        self.performSegue(withIdentifier: "login", sender: self)
    }
    
    func deleteLogoutFailure(error: String?) {
        
        SVProgressHUD.dismiss()
        
        if let error = error {
            
            let alert = UIAlertController(title: "Atenção", message: error, preferredStyle: .alert)
            
            let closeAction = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            
            alert.addAction(closeAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func deleteContatoSuccess(indexPath: IndexPath) {
        
        //obter o contato que deve ser excluído.
        let contato = self.contatos[indexPath.section][indexPath.row]
        
        // remover o contato do array da tela.
        self.contatos[indexPath.section].remove(at: indexPath.row)
        
        // remover da tableview usando animações
        if self.tableView.numberOfRows(inSection: indexPath.section) == 1 { //se o contato for o único na section, atualizar a section.
            
            self.tableView.reloadSections(IndexSet([indexPath.section]), with: .automatic)
            
        } else {
            
            self.tableView.deleteRows(at: [indexPath], with: .automatic) //remover somente o contato.
        }

        // remover o contato do banco de dados local.
        Contato.delete(id: contato.id.value!)
    }
    
    func deleteContatoFailure(error: String?) {
        
        if let error = error {
            
            let alert = UIAlertController(title: "Atenção", message: error, preferredStyle: .alert)
            
            let closeAction = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            
            alert.addAction(closeAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "editarContato" {
            
            if let controller = segue.destination as? EditarContatoViewController {
                
                let contato = sender as! Contato
                
                controller.contato = contato
            }
        }
    }
}

