//
//  RequestFactory.swift
//  ListaContatos
//
//  Created by administrador on 24/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import Foundation
import Alamofire

var baseURL = "https://treinamentoios.herokuapp.com"

class RequestFactory {
   
    static private func getHeaders() -> HTTPHeaders? {
        
        if let usuario = Usuario.get() {
            
            let headers : HTTPHeaders = ["Access-Token": usuario.token, "Client": usuario.client, "Uid": usuario.email]
            
            return headers
        }
        
        return nil
    }
    
    static func postCriarUsuario(email: String, senha: String) -> DataRequest {
        
        let parameters : Parameters = ["email":email, "password_confirmation": senha, "password": senha]
        
        return Alamofire.request(baseURL + "/api/v1/auth", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }

    static func postLogin(email: String, senha: String) -> DataRequest {
        
        let parameters : Parameters = ["email": email, "password": senha]
        
        return Alamofire.request(baseURL + "/api/v1/auth/sign_in", method: .post, parameters: parameters, encoding: JSONEncoding.default)
    }
    
    static func deleteLogout(email: String) -> DataRequest {
        
        let parameters : Parameters = ["email": email]
        
        return Alamofire.request(baseURL + "/api/v1/auth/sign_out", method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: getHeaders())
    }
    
    static func postCriarContato (contato: Contato) -> DataRequest{
        
        contato.usuario = (Usuario.get()?.id)!
        
        return Alamofire.request(baseURL + "/api/v1/contacts", method: .post, parameters: contato.toJSON(), encoding: JSONEncoding.default, headers: getHeaders())
    }
    
    static func getContatos () -> DataRequest {
        
        return Alamofire.request(baseURL + "/api/v1/contacts", method: .get, headers: getHeaders())
    }
    
    static func putEditarContato (contato: Contato) -> DataRequest {
        
        let url = baseURL + "/api/v1/contacts/\(String(describing: contato.id.value!))"
        
        return Alamofire.request(url, method: .put, parameters: contato.toJSON(), encoding: JSONEncoding.default, headers: getHeaders())
    }
    
    static func deleteContato (id: Int) -> DataRequest {

        return Alamofire.request(baseURL + "/api/v1/contacts/\(id)", method: .delete, headers: getHeaders())
    }
}
