//
//  ContatoTableViewCell.swift
//  ListaContatos
//
//  Created by administrador on 26/07/17.
//  Copyright © 2017 administrador. All rights reserved.
//

import UIKit

class ContatoTableViewCell: UITableViewCell {

    @IBOutlet weak var imageContato: UIImageView!
    @IBOutlet weak var nomeContato: UILabel!
    @IBOutlet weak var numeroContato: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imageContato.layer.cornerRadius = self.imageContato.frame.size.width/2
        self.imageContato.clipsToBounds = true
        
        self.imageContato.layer.borderColor = UIColor.lightGray.cgColor
        self.imageContato.layer.borderWidth = 0.5
    }
}
